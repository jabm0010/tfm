package ujaen.spslidar.services.core;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ujaen.spslidar.DTOs.http.DatablockDTO;
import ujaen.spslidar.Exceptions.DifferentUTMZone;
import ujaen.spslidar.entities.AbstractDatablock;
import ujaen.spslidar.entities.Datablock;
import ujaen.spslidar.entities.GeorefBox;
import ujaen.spslidar.repositories.DatablockRepositoryInterface;
import ujaen.spslidar.repositories.DatasetRepositoryInterface;
import ujaen.spslidar.repositories.FileRepositoryInterface;
import ujaen.spslidar.repositories.mongo.GridFileStorageService;
import ujaen.spslidar.services.tools.LasToolsService;
import ujaen.spslidar.services.tools.SystemFileStorageService;

import java.io.IOException;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

@Service
public class DatablockServiceCommonUtils {

    Logger logger = LoggerFactory.getLogger(DatablockServiceCommonUtils.class);

    private DatasetRepositoryInterface datasetRepositoryInterface;
    private DatablockRepositoryInterface datablockRepositoryInterface;
    private FileRepositoryInterface fileRepositoryInterface;
    private SystemFileStorageService systemFileStorageService;
    private LasToolsService lasToolsService;

    public DatablockServiceCommonUtils(DatasetRepositoryInterface datasetRepositoryInterface,
                                       DatablockRepositoryInterface datablockRepositoryInterface,
                                       FileRepositoryInterface fileRepositoryInterface,
                                       SystemFileStorageService systemFileStorageService,
                                       LasToolsService lasToolsService) {

        this.datablockRepositoryInterface = datablockRepositoryInterface;
        this.fileRepositoryInterface = fileRepositoryInterface;
        this.systemFileStorageService = systemFileStorageService;
        this.lasToolsService = lasToolsService;
        this.datasetRepositoryInterface = datasetRepositoryInterface;
    }

    /**
     * Checks if a datablock exists in the database
     *
     * @param workspaceName
     * @param datasetName
     * @param id
     * @return
     */
    public Mono<Boolean> dataBlockExists(String workspaceName, String datasetName, int id, String southWest, String northEast) {
        GeorefBox grid = new GeorefBox(southWest, northEast);

        return datablockRepositoryInterface.existsByWorkspaceAndDatasetAndNodeAndGridCell(workspaceName, datasetName, id, grid);
    }

    /**
     * Get metadata of a datablock
     *
     * @param workspaceName
     * @param datasetName
     * @param id
     * @param southWest
     * @param northEast
     * @return
     */
    public Flux<DatablockDTO> getDatablockData(String workspaceName, String datasetName, int id, String southWest, String northEast) {

        Flux<AbstractDatablock> datablockFlux;

        if (southWest.equals("") || northEast.equals("")) {
            datablockFlux = datablockRepositoryInterface
                    .findDatablockByWorkspaceAndDatasetAndNode(workspaceName, datasetName, id);
        } else {
            GeorefBox grid = new GeorefBox(southWest, northEast);
            datablockFlux = Flux.from(datablockRepositoryInterface
                    .findDatablockByWorkspaceAndDatasetAndNodeAndGridCell(workspaceName, datasetName, id, grid));
        }

        return datablockFlux.map(DatablockDTO::new);

    }

    public Flux<DataBuffer> getDatablockFile(String workspaceName, String datasetName, int id, String southWest, String northEast) {
        GeorefBox grid = new GeorefBox(southWest, northEast);

        if (fileRepositoryInterface instanceof GridFileStorageService) {
            Mono<Datablock> datablock = datablockRepositoryInterface
                    .findDatablockByWorkspaceAndDatasetAndNodeAndGridCell(workspaceName, datasetName, id, grid)
                    .cast(Datablock.class);

            return datablock
                    .map(Datablock::getObjectId)
                    .flatMapMany(objectId -> fileRepositoryInterface.getFile(objectId));

        } else {
            return fileRepositoryInterface.getFile(workspaceName, datasetName, id, grid);
        }
    }


    public Flux<DataBuffer> getCompleteDataset(String workspaceName, String datasetName) {

        Flux<ObjectId> files = datablockRepositoryInterface
                .findAllDatablocksInDataset(workspaceName, datasetName)
                .cast(Datablock.class)
                .map(Datablock::getObjectId);

        return mergeFiles(files, workspaceName, datasetName);

    }


    public Flux<DatablockDTO> getDatablocksByRegion(String workspaceName, String datasetName, String southWest, String northEast) {
        return getOverlappingDatablocks(workspaceName, datasetName, southWest, northEast)
                .map(DatablockDTO::new);

    }

    public Flux<DataBuffer> getFilesByRegion(String workspaceName, String datasetName, String southWest, String northEast, Boolean merged) {

        if (!merged)
            return getOverlappingDatablocks(workspaceName, datasetName, southWest, northEast)
                    .map(AbstractDatablock::getObjectId)
                    .flatMap(fileRepositoryInterface::getFile);
        else {
            Flux<ObjectId> dblocksFilesIDs = getOverlappingDatablocks(workspaceName, datasetName, southWest, northEast)
                    .map(AbstractDatablock::getObjectId);
            return this.mergeFiles(dblocksFilesIDs, workspaceName, datasetName);
        }
    }


    /**
     * Returns all the datablocks that fit the spatial query
     *
     * @param workspaceName
     * @param datasetName
     * @param southWest
     * @param northEast
     * @return
     */
    private Flux<AbstractDatablock> getOverlappingDatablocks(String workspaceName, String datasetName, String southWest, String northEast) {

        GeorefBox queryBox = new GeorefBox(southWest, northEast);
        if (!queryBox.getSouthWestBottom().getZone().equals(queryBox.getNorthEastTop().getZone()))
            return Flux.error(new DifferentUTMZone());

        return datasetRepositoryInterface.findByWorkspaceAndDataset(workspaceName, datasetName)
                .flatMapMany(dataset -> {
                    List<GeorefBox> georefBoxList = dataset.getRootDatablocks().getOrDefault(queryBox.getSouthWestBottom().getZone(), new ArrayList<>());
                    return Flux.fromIterable(georefBoxList);
                })
                .filter(georefBox -> georefBox.doesOverlap(queryBox))
                .flatMap(georefBox -> datablockRepositoryInterface.findDatablockByWorkspaceAndDatasetAndNodeAndGridCell(workspaceName, datasetName, 0, georefBox))
                .flatMap(datablock -> checkOverlappingChildren(datablock, workspaceName, datasetName, queryBox));
    }

    /**
     * Recursive method to explore the octree and recover overlapping datablocks
     *
     * @param datablock
     * @param workspaceName
     * @param datasetName
     * @param comparingCell
     * @return
     */
    private Flux<AbstractDatablock> checkOverlappingChildren(AbstractDatablock datablock, String workspaceName, String datasetName, GeorefBox comparingCell) {

        Flux<AbstractDatablock> abstractDatablockFlux = Flux.empty();

        return abstractDatablockFlux
                .concatWith(Flux.fromIterable(datablock.getChildren())
                        .flatMap(integer -> datablockRepositoryInterface
                                .findDatablockByWorkspaceAndDatasetAndNodeAndGridCell(workspaceName, datasetName, integer, datablock.getUTMZoneLocalGrid()))
                        .filter(_datablock -> comparingCell.doesOverlap(_datablock.getGeorefBox()))
                        .flatMap(_datablock -> checkOverlappingChildren(_datablock, workspaceName, datasetName, comparingCell))
                ).concatWith(Mono.just(datablock));

    }


    private Flux<DataBuffer> mergeFiles(Flux<ObjectId> files, String workspaceName, String datasetName) {
        Path folderToMerge = Path.of(systemFileStorageService.buildMergeDirectory(workspaceName, datasetName));
        Path fileToReturn = folderToMerge.resolve(Path.of("merged.laz"));

        return files
                .flatMap(objectId -> {
                    Path resourceFileName = folderToMerge.resolve(Path.of(objectId.toString() + ".laz"));
                    try {
                        AsynchronousFileChannel asynchronousFileChannel =
                                AsynchronousFileChannel.open(resourceFileName, StandardOpenOption.CREATE, StandardOpenOption.WRITE);

                        return DataBufferUtils.write(fileRepositoryInterface.getFile(objectId), asynchronousFileChannel)
                                .map(DataBufferUtils::release)
                                .then(Mono.just(String.valueOf(resourceFileName)));
                    } catch (IOException ioException) {
                        return Flux.error(new RuntimeException("Couldn't merge files"));
                    }
                })
                .collectList()
                .flatMap(strings -> lasToolsService.mergeFilesReturn(strings, fileToReturn)
                        .map(FileSystemResource::new))
                .flatMapMany(fileSystemResource -> DataBufferUtils
                        .read(fileSystemResource, new DefaultDataBufferFactory(), 256))
                .doAfterTerminate(() -> systemFileStorageService.cleanDirectory(folderToMerge));


    }

}