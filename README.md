SPSLidar app

Dependencies needed:
- Java 11
- Python 3.8 
- LasTools (https://rapidlasso.com/lastools/)
- Mongo 4.2

Run project as a maven app

Once the project has started, run the test code provided in the Python app SPSLTests.

Modify the paths assigned to the location where the LAZ files have been stored in the TestsComplete.py script.
Add or remove from the datasets[] array the datasets that want to be tested.
Add or remove from the testsMaxDatablockSize[] array the values that want to be tested as maximum datablock value.
Add or remove from the testsMaxOctreeSizes[] array the values that want to be tested as the max depth level for the octrees to be created.

A product of all these arrays then will be done and used as the parameters combinations. The script will run all the tests defined, showing at the end the
stats obtained from the insertion and querying process.


Concurrent tests can be done using npm loadtests (https://www.npmjs.com/package/loadtest)

Run the command "loadtest [-n requests] [-c concurrency] URL", making sure that there is at least a dataset in the system.
For the tests performed in the article, the first dataset was used, with different maximum datablock values (10.000, 100.000 y 1.000.000)

Example of retrieving 1000 files for 10 concurrent users using the root datablock of one of the octrees (which will have a number of points similar to the maximum datablock value defined)
loadtest -n 1000 -c 10 "http://localhost:8080/spslidar/workspaces/Navarra/datasets/City of Pamplona/datablocks/0/data?sw_coord=30N6000004740000&ne_coord=30N6100004750000"